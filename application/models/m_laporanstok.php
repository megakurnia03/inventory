<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_laporanstok extends CI_Model {

	var $table = 't_barang';

	$this->db->select('t_barang.id,nama_barang, nama_jenis,nama_kategori,nama_sub_kategori,nama_merk,harga_satuan,harga_grosir,stok,keterangan');
	$this->db->from('t_barang');
	$this->db->join('t_jenis_barang','t_barang.jenis_barang=t_jenis_barang.id','left');
	$this->db->join('t_kategori','t_barang.kategori=t_kategori.id','left'); 
	$this->db->join('t_sub_kategori','t_barang.sub_kategori=t_sub_kategori.id','left');
	$this->db->join('t_merk','t_barang.merk=t_merk.id','left');
	$this->db->order_by('id','desc');
	$query = $this->db->get();
	return $query->result();
}



}

/* End of file m_laporanstok.php */
/* Location: ./application/models/m_laporanstok.php */