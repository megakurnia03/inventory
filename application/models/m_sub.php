<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sub extends CI_Model {

	var $table = 't_sub_kategori';

	public function add_sub($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	// public function get_all_sub(){
	// 	$this->db->select('t_sub_kategori.id, nama_sub_kategori, nama_kategori');
	// 	$this->db->from('t_sub_kategori');
	// 	$this->db->join('t_kategori', 't_sub_kategori.id_kategori=t_kategori.id', 'left');
	// 	$this->db->order_by('id','desc');
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }

	public function get_all_sub(){
		$query = $this->db->query('SELECT t_sub_kategori.id, nama_sub_kategori, nama_kategori from t_sub_kategori inner join t_kategori on t_sub_kategori.id_kategori=t_kategori.id order by id desc');
		return $query->result();
	}


	public function get_by_id($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_sub($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_kategori(){
		$query = $this->db->get('t_kategori');
		return $query;
	}

}

/* End of file m_sub.php */
/* Location: ./application/models/m_sub.php */