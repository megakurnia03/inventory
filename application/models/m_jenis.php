<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis extends CI_Model {

	var $table = 't_jenis_barang';

	public function get_all_jenis() {
		$this->db->from('t_jenis_barang');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_jenis($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_jenis($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}
	
	function get_namjen($nama_jenis){
        $this->db->from($this->table);
        $query = $this->db->where('nama_jenis' , $nama_jenis);
        return $query->get()->result();
    }

}

/* End of file m_jenis.php */
/* Location: ./application/models/m_jenis.php */