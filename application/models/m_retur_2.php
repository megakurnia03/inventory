<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_retur_2 extends CI_Model {

	var $table = 't_retur';

	public function get_all(){

		$this->db->select('t_retur.id,nama_barang, nama_merk, nama_supplier, tgl_retur, no_retur, jumlah, t_retur.keterangan');
		$this->db->from('t_retur');
		$this->db->join('t_barang','t_retur.id_barang=t_barang.id','left');
		$this->db->join('t_merk','t_retur.merk=t_merk.id','left');
		$this->db->join('t_supplier','t_retur.id_supplier=t_supplier.id','left');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_retur($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function edit_retur($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_retur($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_supplier(){
		$query = $this->db->get('t_supplier');
		return $query;
	}

	function get_barang(){
		$query = $this->db->get('t_barang');
		return $query;
	}

	function get_merk(){
		$query = $this->db->get('t_merk');
		return $query;
	}

	function get_nama_merk($merk){
		$query = $this->db->get_where('t_barang', array('merk' => $merk));
		return $query;
	}
	

}

/* End of file m_retur_2.php */
/* Location: ./application/models/m_retur_2.php */