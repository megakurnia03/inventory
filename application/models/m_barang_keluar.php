<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang_keluar extends CI_Model {

	var $table = 't_barang_keluar';

	public function get_all(){
		$this->db->select('t_barang_keluar.id, nama_user, nama_barang, jumlah, tgl_keluar, no_ref, t_barang_keluar.keterangan');
		$this->db->from('t_barang_keluar');
		$this->db->join('t_barang', 't_barang_keluar.id_barang=t_barang.id', 'left');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_bk($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function edit_bk($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}
	
	public function update_bk($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	// function get_user(){
	// 	$query = $this->db->get('t_user');
	// 	return $query;
	// }

	function get_barang(){
		$query = $this->db->get('t_barang');
		return $query;
	}

	public function panel_bkeluar(){
		$query = $this->db->query("SELECT SUM(jumlah) AS jumlah_keluar FROM 
			t_barang_keluar");
		return $query->row_array();
	}

	function get_jumlah_barang($id_barang){
		$query = $this->db->get_where('t_barang', array('id' => $id_barang));
		return $query;
	}
	

}

/* End of file m_barang_keluar.php */
/* Location: ./application/models/m_barang_keluar.php */