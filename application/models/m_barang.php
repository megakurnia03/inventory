<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {

	var $table = 't_barang';

	public function add_barang($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_all_barang(){
		$this->db->select('t_barang.id,nama_barang, nama_jenis,nama_kategori,nama_sub_kategori,nama_merk,harga_satuan,harga_grosir,stok,keterangan');
		$this->db->from('t_barang');
		$this->db->join('t_jenis_barang','t_barang.jenis_barang=t_jenis_barang.id','left');
		$this->db->join('t_kategori','t_barang.kategori=t_kategori.id','left'); 
		$this->db->join('t_sub_kategori','t_barang.sub_kategori=t_sub_kategori.id','left');
		$this->db->join('t_merk','t_barang.merk=t_merk.id','left');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_jumlah(){
		$query = $this->db->query('SELECT * FROM t_barang WHERE stok<10');
		return $query;
	}

	public function get_by_id($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_barang($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_kategori(){
		$query = $this->db->get('t_kategori');
		return $query;
	}

	function get_jenis(){
		$query = $this->db->get('t_jenis_barang');
		return $query;
	}

	function get_sub_kategori($id_kategori){
		$query = $this->db->get_where('t_sub_kategori', array('id_kategori' => $id_kategori));
		return $query;
	}

	function get_merk(){
		$query = $this->db->get('t_merk');
		return $query;
	}

	function get_nambar($nama_barang){
        $this->db->from($this->table);
        $query = $this->db->where('nama_barang' , $nama_barang);
        return $query->get()->result();
    }

    public function panel_barang(){
		$query = $this->db->query("SELECT SUM(stok) AS stok_barang FROM 
			t_barang");
		return $query->row_array();
	}

	function get_barang($id_barang){
		$query=$this->db->query("SELECT * FROM t_barang where id='$id_barang'");
		return $query;
	}


}

/* End of file m_barang.php */
/* Location: ./application/models/m_barang.php */