<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_merk extends CI_Model {

	var $table = 't_merk';

	public function get_all_merk() {
		$this->db->from('t_merk');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_merk($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_merk($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_namer($nama_merk){
		$this->db->from($this->table);
		$query = $this->db->where('nama_merk' , $nama_merk);
		return $query->get()->result();
	}

}

/* End of file m_merk.php */
/* Location: ./application/models/m_merk.php */