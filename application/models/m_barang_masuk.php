<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang_masuk extends CI_Model {

	var $table = 't_barang_masuk';

	public function get_all(){
		$this->db->select('t_barang_masuk.id, nama_user, nama_supplier, nama_barang, jumlah, tgl_masuk, no_ref');
		$this->db->from('t_barang_masuk');
		$this->db->join('t_supplier', 't_barang_masuk.id_supplier=t_supplier.id', 'left');
		$this->db->join('t_barang', 't_barang_masuk.id_barang=t_barang.id', 'left');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_bm($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function edit_bm($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}
	
	public function update_bm($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	// function get_user(){
	// 	$query = $this->db->get('t_user');
	// 	return $query;
	// }

	function get_supplier(){
		$query = $this->db->get('t_supplier');
		return $query;
	}

	function get_barang(){
		$query = $this->db->get('t_barang');
		return $query;
	}

	public function panel_bmasuk(){
		$query = $this->db->query("SELECT SUM(jumlah) AS jumlah_masuk FROM 
			t_barang_masuk");
		return $query->row_array();
	}

	public function view_by_date($date){
        $this->db->where('DATE(tgl_masuk)', $date); // Tambahkan where tanggal nya
    	return $this->db->get('t_barang_masuk')->result();// Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
  	}

  	public function view_by_month($month, $year){
        $this->db->where('MONTH(tgl_masuk)', $month); // Tambahkan where bulan
        $this->db->where('YEAR(tgl_masuk)', $year); // Tambahkan where tahun
    	return $this->db->get('t_barang_masuk')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
  	}

  	 public function view_by_year($year){
        $this->db->where('YEAR(tgl_masuk)', $year); // Tambahkan where tahun
    	return $this->db->get('t_barang_masuk')->result(); // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
  	}

  	public function view_all(){

    return $this->db->get('t_barang_masuk')->result(); // Tampilkan semua data transaksi

  	}

  	public function option_tahun(){
        $this->db->select('YEAR(tgl_masuk) AS tahun'); // Ambil Tahun dari field tgl
        $this->db->from('t_barang_masuk'); // select ke tabel transaksi
        $this->db->order_by('YEAR(tgl_masuk)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
        $this->db->group_by('YEAR(tgl_masuk)'); // Group berdasarkan tahun pada field tgl
        
        return $this->db->get()->result(); // Ambil data pada tabel transaksi sesuai kondisi diatas
    }

}

/* End of file m_barang_masuk.php */
/* Location: ./application/models/m_barang_masuk.php */