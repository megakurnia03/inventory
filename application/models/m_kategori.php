<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategori extends CI_Model {

	var $table = 't_kategori';

	public function get_all_kategori() {
		$this->db->from('t_kategori');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_kategori($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_kategori($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_namkat($nama_kategori){
        $this->db->from($this->table);
        $query = $this->db->where('nama_kategori' , $nama_kategori);
        return $query->get()->result();
    }

}

/* End of file m_kategori.php */
/* Location: ./application/models/m_kategori.php */