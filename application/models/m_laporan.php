<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_laporan extends CI_Model {

	public function view_by_date($date){
        
        $this->db->where('DATE(tgl)', $date); // Tambahkan where tanggal nya
        
    	return $this->db->get('transaksi')->result();// Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
}

}

/* End of file m_laporan.php */
/* Location: ./application/models/m_laporan.php */