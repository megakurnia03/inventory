<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

    var $table = 't_user';

    public function add_user($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function get_all_user() {
        $this->db->from('t_user');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_id($id){
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function update_user($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
    
    public function delete_by_id($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    function get_username($username){
        $this->db->from($this->table);
        $query = $this->db->where('username' , $username);
        return $query->get()->result();
    }

}

