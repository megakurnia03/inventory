<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_supplier extends CI_Model {

	var $table = 't_supplier';

	public function get_all_supplier() {
		$this->db->from('t_supplier');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_supplier($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id){
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update_supplier($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

}

/* End of file m_supplier.php */
/* Location: ./application/models/m_supplier.php */