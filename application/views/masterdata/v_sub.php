<div class="container-fluid">
	<!-- Page Heading -->
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="#">Home </a>
			</li>
			<li class="active">
				<i class="ace-icon fa fa-angle-double-right"></i>
			</li>
			<li class="active">
				Master Data
			</li>
			<li class="active">
				<i class="ace-icon fa fa-angle-double-right"></i>
			</li>
			<li class="active">
				Data Sub Kategori
			</li>
		</ul>
	</div>
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="container-fluid">
			<div class="d-sm-flex align-items-center justify-content-between mb-2">
				<button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onclick="add_sub()" ><i class="fas fa-plus-circle fa-sm text-white-50"></i> Tambah Sub Kategori </button>
			</div>
		</div>
		<!-- DataTales Example -->
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Data Sub Kategori</h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" id="table" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>Nama Sub</th>
								<th>Kategori</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody id="show_data">
							<?php
							foreach ($t_sub_kategori as $sub) {
								?>
								<tr>
									<td><?php echo $sub->nama_sub_kategori;?></td>
									<td><?php echo $sub->nama_kategori;?></td>
									<td>
										<button class="btn btn-primary" onclick="edit_sub(<?php echo $sub->id;?>)">Edit</button>
										<button class="btn btn-warning" onclick="delete_sub(<?php echo $sub->id;?>)">hapus</button>
									</td>
								</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Nama</th>
									<th>Kategori</th>
									<th>Aksi</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade in" id="modal_form" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg modal-right modal-notify modal-info" role="document">
				<div class="modal-content">
					<div class="card shadow mb-6">
						<!--Header-->
						<div class="modal-header no-padding bg-primary">
							<h5 class="modal-tittle m-0 font-weight-bold text-light">
								Input Sub Kategori Barang
							</h5>
						</div>
						<!--End Header-->
						<!--Modal Body-->
						<div class="modal-body no-padding">
							<form action="#" id="form" method="">
								<input type="hidden" name="id" id="id">
								<div class="col-md-12">
									<div class="form-group row">
										<div class="col-sm-12">
											<label class="control-label">Nama :</label>
											<input type="text" class="form-control" placeholder="Masukan nama Sub Kategori..." name="nama_sub_kategori" id="nama_sub_kategori">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group row">
										<div class="col-sm-12">
											<label class="control-label">Kategori :</label>
											<select class="form-control" name="id_kategori" id="id_kategori">
												<option value="">No Selected</option>
												<?php foreach($t_kategori as $row):?>
													<option value="<?php echo $row->id;?>"><?php echo $row->nama_kategori;?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<div class="form-group">
								<button type="submit" id="save" name="save" onclick="simpan()" class="btn btn-outline-primary btn-bold"><i class="fa fa-check"></i>Simpan</button>
								<button type="submit" name="cancel" onclick="Batal()" data-dismiss="modal" class="btn btn-outline-danger btn-bold"><i class="fa fa-times" aria-hidden="true"></i>Batal</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#table').DataTable();
});

function validasi_modal(){
	var nama_sub_kategori = $('#nama_sub_kategori').val();
	var id_kategori = $('#id_kategori').val();
	if (nama_sub_kategori == '') {
		swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
		return false;
	}
	if (id_kategori == '') {
		swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
		return false;
	}

	return true;
}

var save_method;
var table;

function add_sub() {
	save_method = 'add';
	$('#form')[0].reset();
	$('#modal_form').modal('show');
}

function Batal(){
	location.reload();
}

function simpan() {
	if(!validasi_modal()){
		return ;
	}
	var url;

	if(save_method == 'add') {
		url = '<?php echo site_url('sub_kategori/add_sub');?>';
	} else {
		url = '<?php echo site_url('sub_kategori/update_sub');?>';
	}

	$.ajax({
		url: url,
		type: "POST",
		data: $('#form').serialize(),
		dataType: "JSON",
		success: function(data){
			if(!data['status']){
				swal("Gagal", data['message'], "error")
			}else{
				$('#modal_form').modal('hide');
				swal({
					title : "",
					type : "success",
					text : "Data berhasil disimpan",
					confirmButtonText : "Oke"
				}, 
				function() {
					location.reload();
				});   
			}
		},

		error: function(jqXHR, textStatus, errorThrown) {
			alert('Error Adding / Update Data');
		}
	});
}

function delete_sub(id){
	swal({
		text : 'Apakah anda yakin akan menghapus data ?',
		type : 'warning',
		title : 'Hapus Data',
		showCancelButton : true,
		cancelButtonText : "Batal",
		confirmButtonText : "Hapus",
		closeOnConfirm : false,
	},function (isConfirm) {
		if (!isConfirm) return;
		$.ajax({
			url: "<?php echo site_url('sub_kategori/delete_sub');?>/"+id,
			type: "POST",
			dataType: "JSON",
			success: function(data){
				swal({
					title : "",
					type : "success",
					text : "Data berhasil di hapus",
					confirmButtonText : "Oke"
				}, 
				function() {
					location.reload();
				});
			},
			error: function (jqXHR, textStatus, errorThrown){
				swal("Error", "Data gagal dihapus !", "error");
			}
		});

	})
}

function edit_sub(id){
	save_method = 'update';
	$('#form')[0].reset();

	//load data dari ajax
	$.ajax({
		url: "<?php echo site_url('sub_kategori/ajax_edit/');?>/"+id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="id"]').val(data.id);
			$('[name="nama_sub_kategori"]').val(data.nama_sub_kategori);
			$('[name="id_kategori"]').val(data.id_kategori);
			$('#modal_form').modal('show');
			$('.modal-tittle').text('Edit kategori barang');
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert('Error get data from ajax');
		}

	});
}
</script>	
