<div class="container-fluid">
	<!-- Page Heading -->
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="#">Home </a>
			</li>
			<li class="active">
				<i class="ace-icon fa fa-angle-double-right"></i>
			</li>
			<li class="active">
				Master Data
			</li>
			<li class="active">
				<i class="ace-icon fa fa-angle-double-right"></i>
			</li>
			<li class="active">
				Data Pegawai
			</li>
		</ul>
	</div>
	<div class="container-fluid">
		<!-- Page Heading -->
		<div class="container-fluid">
			<div class="d-sm-flex align-items-center justify-content-between mb-2">
				<button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onclick="add_user()" ><i class="fas fa-plus-circle fa-sm text-white-50"></i> Tambah User </button>
				<!-- <button class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" onclick="reload_table()" ><i class="fa-sm text-white-50"></i> Reload </button> -->
			</div>
			<!--<div class="d-sm-flex align-items-center justify-content-between mb-2">
				<a href="#AddUser" data-toggle="modal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus-circle fa-sm text-white-50"></i> Tambah User </a>
			</div>-->
			<!-- DataTales Example -->
			<div class="card shadow mb-4">
				<div class="card-header py-3">
					<h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="mydatauser" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Nama</th>
									<th>Jenis Kelamin</th>
									<th>Alamat</th>
									<th>Nomor Telepon</th>
									<th>Username</th>
									<th>Password</th>
									<th>Level</th>
									<th>Email</th>
									<th style="width: 175px">Aksi</th>
								</tr>
							</thead>
							<tbody id="show_data">
								<?php
								foreach ($t_user as $user) {
									?>
									<tr>
										<td><?php echo $user->nama_user;?></td>
										<td><?php echo $user->jenis_kelamin;?></td>
										<td><?php echo $user->alamat;?></td>
										<td><?php echo $user->no_telp;?></td>
										<td><?php echo $user->username;?></td>
										<td><?php echo $user->password;?></td>
										<td><?php echo $user->level;?></td>
										<td><?php echo $user->email;?></td>
										<td>
											<button class="btn btn-primary" onclick="edit_user(<?php echo $user->id;?>)">Edit</button>
											<button class="btn btn-warning" onclick="delete_user(<?php echo $user->id;?>)">hapus</button>
										</td>
									</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th>Nama</th>
										<th>Jenis Kelamin</th>
										<th>Alamat</th>
										<th>Nomor Telepon</th>
										<th>Username</th>
										<th>Password</th>
										<th>Level</th>
										<th>Email</th>
										<th style="width: 175px">Aksi</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Modal-->
	<div class="modal fade" id="modal_form" role="dialog">
		<div class="modal-dialog modal-lg modal-right ">
			<div class="modal-content">
				<!--Header-->
				<div class="modal-header no-padding bg-primary">
					<h5 class="modal-tittle m-0 font-weight-bold text-light">
						Input User
					</h5>
				</div>
				<!--End Header-->
				<!--Modal Body-->
				<div class="modal-body no-padding form">
					<form action="#" id="form-modal" class="form horizontal" >
						<input type="hidden" name="id">

						<div class="form-body">
							<div class="col-md-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label class="control-label" for="nama">Nama :</label>
										<input type="text" class="form-control" name="nama_user" id="nama_user" placeholder="Masukan Nama Lengkap">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label class="control-label" for="jenis_kelamin">Jenis Kelamin :</label>
										<select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
											<option>Laki-laki</option>
											<option>Perempuan</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label class="control-label" for="alamat">Alamat :</label>
										<textarea class="form-control" rows="4" name="alamat" id="alamat"></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label class="control-label" for="no_telp">No Telp :</label>
										<input type="number" class="form-control" name="no_telp" id="no_telp" placeholder="Masukan Nomor Telepon">
									</div>
								</div>
							</div>
							<script>
								function validasiNotelp(){

								}
							</script>
							<div class="col-md-12">
								<div class="form-group row">
									<div class="col-sm-6">
										<label class="control-label" for="username">Username :</label>
										<input type="text" class="form-control" name="username" id="username" placeholder="Username...">
									</div>
									<div class="col-sm-6">
										<label class="control-label" for="password">Password :</label>
										<input type="text" class="form-control" name="password" id="password" placeholder="Password...">
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group row">
									<div class="col-sm-2">
										<label class="control-label">Level :</label>
										<select class="form-control" name="level" id="level">
											<option>1</option>
											<option>2</option>
										</select>
									</div>
									<div class="col-sm-10">
										<label class="control-label">Email :</label>
										<input type="text" class="form-control" placeholder="Masukan email" name="email" id="email">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">

						<button type="button" id="save" name="save" onclick="Simpan()" class="btn btn-outline-primary btn-bold"><i class="fa fa-check"></i>Simpan</button>
						<button type="button" name="cancel" data-dismiss="modal" onclick="Batal()" class="btn btn-outline-danger btn-bold"><i class="fa fa-times" aria-hidden="true"></i>Batal</button>

					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
	<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
	<!--<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>-->
	<script type="text/javascript">

	$(document).ready(function() {
		$('#mydatauser').DataTable();
	} );

	$('#form-modal').submit(function(e){
		return false;
	});

	function Batal(){
		location.reload();
	}

	function validasi_modal(){
		var nama = $('#nama_user').val();
		var alamat = $('#alamat').val();
		var jk = $('#jenis_kelamin').val();
		var np = $('#no_telp').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var level = $('#level').val();
		var email = $('#email').val();
		if (nama == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (alamat == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (jk == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (np == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (username == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (password == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (level == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (email == '') {
			swal({
			text : 'Seluruh Data Wajib Diisi',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (np.length != 12 && np.length != 13) {
			swal({
			text : 'no telpon tidak valid',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} if (np < '0') {
			swal({
			text : 'no telpon tidak valid',
			type : 'error',
			title : 'Tidak Bisa Menyimpan',
			showCancelButton : false,
				//cancelButtonText : "Batal",
				confirmButtonText : "Ok",
				closeOnConfirm : false,
			})
			return false;
		} 

		return true;
	}


	var save_method;
	var table;

	function add_user() {
		save_method = 'add';
		$('#form-modal')[0].reset();
		$('#modal_form').modal('show');
	}

	function Simpan() {
		if(!validasi_modal()){
			return ;
		}
		var url;

		if(save_method == 'add') {
			url = '<?php echo site_url('User/add_user');?>';
		} else {
			url = '<?php echo site_url('User/update_user');?>';
		}

		$.ajax({
			url: url,
			type: "POST",
			data: $('#form-modal').serialize(),
			dataType: "JSON",
			success: function(data){
				if(!data['status']){
					swal("Gagal", data['message'], "error")
				}else{
					$('#modal_form').modal('hide');
					swal({
						title : "",
						type : "success",
						text : "Data berhasil disimpan",
						confirmButtonText : "Oke"
					}, 
					function() {
						location.reload();
					});   
				}
			},

			error: function(jqXHR, textStatus, errorThrown) {
				alert('Error Adding / Update Data');
			}
		});
	}

	function edit_user(id){
		save_method = 'update';
		$('#form-modal')[0].reset();

	//load data dari ajax
	$.ajax({
		url: "<?php echo site_url('User/ajax_edit/');?>/"+id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="id"]').val(data.id);
			$('[name="nama_user"]').val(data.nama_user);
			$('[name="jenis_kelamin"]').val(data.jenis_kelamin);
			$('[name="alamat"]').val(data.alamat);
			$('[name="no_telp"]').val(data.no_telp);
			$('[name="username"]').val(data.username);
			$('[name="password"]').val(data.password);
			$('[name="level"]').val(data.level);
			$('[name="email"]').val(data.email);

			$('#modal_form').modal('show');
			$('.modal-tittle').text('Edit User');
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert('Error get data from ajax');
		}

	});
}

function delete_user(id){
	swal({
    text : 'Apakah anda yakin akan menghapus data ?',
    type : 'warning',
    title : 'Hapus Data',
    showCancelButton : true,
    cancelButtonText : "Batal",
    confirmButtonText : "Hapus",
    closeOnConfirm : false,
  },function (isConfirm) {
    if (!isConfirm) return;
        $.ajax({
      url: "<?php echo site_url('user/delete_user');?>/"+id,
      type: "POST",
      dataType: "JSON",
      success: function(data){
        swal({
          title : "",
          type : "success",
          text : "Data berhasil di hapus",
          confirmButtonText : "Oke"
        }, 
        function() {
          location.reload();
        });
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("Error", "Data gagal dihapus !", "error");
      }
    });

  })
}

</script>