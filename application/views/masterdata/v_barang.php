<div class="container-fluid">
  <!-- Page Heading -->
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home </a>
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Master Data
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Data Barang
      </li>
    </ul>
  </div>
  <div class="container-fluid">
    <!-- Page Heading -->
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onclick="add_barang()" ><i class="fas fa-plus-circle fa-sm text-white-50"></i> Tambah Barang </button>
      </div>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Barang</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Jenis Hewan</th>
                <th>Kategori Barang</th>
                <th>Sub Kategori</th>
                <th>Merk</th>
                <th>Harga Satuan</th>
                <th>Harga Grosir</th>
                <th>Stok</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="show_data">
              <?php
              foreach ($t_barang as $barang) {
                ?>
                <tr>
                  <td><?php echo $barang->nama_barang;?></td>
                  <td><?php echo $barang->nama_jenis;?></td>
                  <td><?php echo $barang->nama_kategori;?></td>
                  <td><?php echo $barang->nama_sub_kategori;?></td>
                  <td><?php echo $barang->nama_merk;?></td>
                  <td><?php echo $barang->harga_satuan;?></td>
                  <td><?php echo $barang->harga_grosir;?></td>
                  <td><?php echo $barang->stok;?></td>
                  <td><?php echo $barang->keterangan;?></td>
                  <td>
                    <button class="btn btn-primary" onclick="edit_barang(<?php echo $barang->id;?>)">Edit</button>
                    <button class="btn btn-warning" onclick="delete_barang(<?php echo $barang->id;?>)">hapus</button>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Nama</th>
                  <th>Jenis Barang</th>
                  <th>Kategori Barang</th>
                  <th>Sub Kategori</th>
                  <th>Merk</th>
                  <th>Harga Satuan</th>
                  <th>Harga Grosir</th>
                  <th>Stok</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade right" id="modal_form" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
          <div class="card shadow mb-6">
            <!--Header-->
            <div class="modal-header no-padding bg-primary">
              <h5 class="modal-tittle m-0 font-weight-bold text-light">
                Input Barang
              </h5>
            </div>
            <!--End Header-->
            <!--Modal Body-->
            <div class="modal-body no-padding">
              <form action="#" id="form-modal" method="">
                <input type="hidden" name="id" id="id">
                <div class="col-md-12">
                  <div class="form-group row">
                    <div class="col-sm-12">
                     <label class="control-label">Nama :</label>
                     <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama Barang...">
                   </div>
                 </div>
               </div>
               <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label class="control-label">Jenis Hewan :</label>
                    <select class="form-control" name="jenis_barang" id="jenis_barang">
                     <option value="">No Selected</option>
                     <?php foreach($jenis as $row):?>
                      <option value="<?php echo $row->id;?>"><?php echo $row->nama_jenis;?></option>
                    <?php endforeach;?>
                  </select>
                </div>
                <div class="col-sm-6">
                  <label class="control-label">Kategori :</label>
                  <select class="form-control" name="kategori" id="kategori" required>
                   <option value="">No Selected</option>
                   <?php foreach($t_kategori as $row):?>
                    <option value="<?php echo $row->id;?>"><?php echo $row->nama_kategori;?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group row">
              <div class="col-sm-6">
                <label class="control-label">Sub Kategori :</label>
                <select class="form-control" name="sub_kategori" id="sub_kategori" required>
                 <option value="">No Selected</option>
               </select>
             </div>
             <div class="col-sm-6">
              <label class="control-label">Merk :</label>
              <select class="form-control" name="merk" id="merk">
               <option value="">No Selected</option>
               <?php foreach($merk as $row):?>
                <option value="<?php echo $row->id;?>"><?php echo $row->nama_merk;?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group row">
          <div class="col-sm-6">
            <label class="control-label">harga satuan :</label>
            <input type="number" name="harga_satuan" id="harga_satuan" " class="form-control" placeholder="IDR...">
          </div>
          <div class="col-sm-6">
            <label class="control-label">harga grosir :</label>
            <input type="number" name="harga_grosir" id="harga_grosir" class="form-control" placeholder="IDR...">
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group row">
          <div class="col-sm-12">
            <label class="control-label">Keterangan :</label>
            <textarea class="form-control" rows="4" name="keterangan" id="keterangan"></textarea>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
    <div class="form-group">
      <button type="submit" name="simpan" value="simpan" onclick="Simpan()" class="btn btn-outline-primary btn-bold"><i class="fa fa-check" aria-hidden="true"></i>Simpan</button>
      <button type="button" name="cancel" data-dismiss="modal" onclick="Batal()" class="btn btn-outline-danger btn-bold"><i class="fa fa-times" aria-hidden="true"></i>Batal</button>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function(){


  $('#table').DataTable();
  $('#kategori').change(function(){
    var id=$(this).val();
    $.ajax({
      url: "<?php echo site_url('Barang/get_sub_kategori');?>",
      method: "POST",
      data: {id:id},
      async: true,
      dataType: 'json',
      success: function(data){

        var html = '';
        var i;
        for(i=0; i<data.length; i++){
         html += '<option value='+data[i].id+'>'+data[i].nama_sub_kategori+'</option>';
       }
       $('#sub_kategori').html(html);
     }
   });
    return false;
  });
});

function Batal(){
  location.reload();
}

function showData(id) {
  $.ajax({
    url: '<?php echo site_url('Barang/get_sub_kategori');?>',
    type : 'POST',
    data : { id : id},
    async: true,
    dataType: 'json',
    success: function(data) {
      var html = '';
      var i;
      for(i=0; i<data.length; i++){
       html += '<option value='+data[i].id+'>'+data[i].nama_sub_kategori+'</option>';
     }
     $('#sub_kategori').html(html);
   }
 });
}

$('#form-modal').submit(function(e){
  return false;
});


function validasi_modal(){
  var nama_barang = $('#nama_barang').val();
  var jenis_barang = $('#jenis_barang').val();
  var kategori = $('#kategori').val();
  var sub_kategori = $('#sub_kategori').val();
  var merk = $('#merk').val();
  var harga_satuan = $('#harga_satuan').val();
  var harga_grosir = $('#harga_grosir').val();
  if (nama_barang == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi Kecuali Keterangan',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (jenis_barang == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi Kecuali Keterangan',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (kategori == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi Kecuali Keterangan',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (sub_kategori == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi Kecuali Keterangan',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (merk == '') {
  swal({
    text : 'Seluruh Data Wajib Diisi Kecuali Keterangan',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
  return false;
} if (harga_satuan == '') {
  swal({
    text : 'Seluruh Data Wajib Diisi Kecuali Keterangan',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
  return false;
} if (harga_grosir == '') {
  swal({
    text : 'Seluruh Data Wajib Diisi Kecuali Keterangan',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
  return false;
}

return true;
}

var save_method;
var table;

function add_barang() {
  save_method = 'add';
  $('#form-modal')[0].reset();
  $('#modal_form').modal('show');
}

function Simpan() {
  if(!validasi_modal()){
    return ;
  }
  var url;

  if(save_method == 'add') {
    url = '<?php echo site_url('barang/add_barang');?>';
  } else {
    url = '<?php echo site_url('barang/update_barang');?>';
  }

  $.ajax({
    url: url,
    type: "POST",
    data: $('#form-modal').serialize(),
    dataType: "JSON",
    success: function(data){
      if(!data['status']){
        swal("Gagal", data['message'], "error")
      }else{
       $('#modal_form').modal('hide');
       swal({
        title : "",
        type : "success",
        text : "Data berhasil disimpan",
        confirmButtonText : "Oke"
      }, 
      function() {
        location.reload();
      });   
     }
   },

   error: function(jqXHR, textStatus, errorThrown) {
    alert('Error Adding / Update Data');
  }
});
}

function edit_barang(id){
  save_method = 'update';
  $('#form-modal')[0].reset();

  //load data dari ajax
  $.ajax({
    url: "<?php echo site_url('barang/ajax_edit/');?>/"+id,
    type: "GET",
    dataType: "JSON",
    success: function(data){
      $('[name="id"]').val(data.id);
      $('[name="nama_barang"]').val(data.nama_barang);
      $('[name="jenis_barang"]').val(data.jenis_barang);
      $('[name="kategori"]').val(data.kategori);
      $('[name="sub_kategori"]').val(data.sub_kategori);
      $('[name="merk"]').val(data.merk);
      $('[name="harga_satuan"]').val(data.harga_satuan);
      $('[name="harga_grosir"]').val(data.harga_grosir);
      $('[name="keterangan"]').val(data.keterangan);

      $('#modal_form').modal('show');
      $('.modal-tittle').text('Edit Barang');
      showData(data.kategori);

    },
    error: function (jqXHR, textStatus, errorThrown) {
      alert('Error get data from ajax');
    }

  });
}

function delete_barang(id){

  swal({
    text : 'Apakah anda yakin akan menghapus data ?',
    type : 'warning',
    title : 'Hapus Data',
    showCancelButton : true,
    cancelButtonText : "Batal",
    confirmButtonText : "Hapus",
    closeOnConfirm : false,
  },function (isConfirm) {
    if (!isConfirm) return;
    $.ajax({
      url: "<?php echo site_url('barang/delete_barang');?>/"+id,
      type: "POST",
      dataType: "JSON",
      success: function(data){
        swal({
          title : "",
          type : "success",
          text : "Data berhasil di hapus",
          confirmButtonText : "Oke"
        }, 
        function() {
          location.reload();
        });
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("Error", "Data gagal dihapus !", "error");
      }
    });

  })
}

</script>
