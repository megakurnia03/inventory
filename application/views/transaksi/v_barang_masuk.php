<div class="container-fluid">
  <!-- Page Heading -->
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home </a>
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Transaksi
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Barang Masuk
      </li>
    </ul>
  </div>
  <div class="container-fluid">
    <!-- Page Heading -->
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onclick="add_bm()" ><i class="fas fa-plus-circle fa-sm text-white-50"></i> Tambah Barang Masuk </button>
      </div>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Barang Masuk</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>User</th>
                <th>Supplier</th>
                <th>Barang</th>
                <th>Jumlah</th>
                <th>Tanggal Masuk</th>
                <th>No Referensi</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="show_data">
              <?php
              foreach ($t_barang_masuk as $barang_masuk) {
                ?>
                <tr>
                  <td><?php echo $barang_masuk->nama_user;?></td>
                  <td><?php echo $barang_masuk->nama_supplier;?></td>
                  <td><?php echo $barang_masuk->nama_barang;?></td>
                  <td><?php echo $barang_masuk->jumlah;?></td>
                  <td><?php echo $barang_masuk->tgl_masuk;?></td>
                  <td><?php echo $barang_masuk->no_ref;?></td>
                  <td>
                    <button class="btn btn-primary" onclick="edit_bm(<?php echo $barang_masuk->id;?>)">Edit</button>
                    <button class="btn btn-warning" onclick="delete_bm(<?php echo $barang_masuk->id;?>)">hapus</button>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>User</th>
                  <th>Supplier</th>
                  <th>Barang</th>
                  <th>Jumlah</th>
                  <th>Tanggal Masuk</th>
                  <th>No Referensi</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade right" id="modal_form" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
          <div class="card shadow mb-6">
            <!--Header-->
            <div class="modal-header no-padding bg-primary">
              <h5 class="modal-tittle m-0 font-weight-bold text-light">
               Input Barang Masuk
             </h5>
           </div>
           <!--End Header-->
           <!--Modal Body-->
           <div class="modal-body no-padding">
            <form action="#" id="form-modal" method="">
              <input type="hidden" name="id" id="id">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label class="control-label">User :</label>
                     <input type="text" name="nama_user" id="nama_user" value="<?php echo $this->session->userdata('nama')?>" class="form-control" readonly>
                   <!--  <select name="id_user" id="id_user" class="form-control">
                      <option value="">No Selected</option>
                      <?php foreach($t_user as $row):?>
                        <option value="<?php echo $row->id;?>"><?php echo $row->nama_user;?></option>
                      <?php endforeach;?>
                    </select> -->
                  </div>
                  <div class="col-sm-6">
                    <label class="control-label">Supplier :</label>
                    <select name="id_supplier" id="id_supplier" class="form-control">
                      <option value="">No Selected</option>
                      <?php foreach($t_supplier as $row):?>
                        <option value="<?php echo $row->id;?>"><?php echo $row->nama_supplier;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <label class="control-label">Barang :</label>
                    <select name="id_barang" id="id_barang" class="form-control">
                      <option value="">No Selected</option>
                      <?php foreach($t_barang as $row):?>
                        <option value="<?php echo $row->id;?>"><?php echo $row->nama_barang;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-sm-3">
                    <label class="control-label">Jumlah :</label>
                    <input type="number" name="jumlah" id="jumlah" class="form-control">
                  </div>
                  <div class="col-sm-4">
                    <label class="control-label">Tanggal Masuk :</label>
                    <input type="Date" name="tgl_masuk" id="tgl_masuk" class="form-control">
                  </div>
                  <div class="col-sm-5">
                    <label class="control-label">No Referensi :</label>
                    <input type="text" name="no_ref" id="no_ref" class="form-control">
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="form-group">
              <button type="submit" name="simpan" value="simpan" onclick="Simpan()" class="btn btn-outline-primary btn-bold"><i class="fa fa-check" aria-hidden="true"></i>Simpan</button>
              <button type="button" name="cancel" onclick="Batal()" data-dismiss="modal" class="btn btn-outline-danger btn-bold"><i class="fa fa-times" aria-hidden="true"></i>Batal</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#table').DataTable();

    // $('.datepicker').datepicker({
    //     autoclose: true,
    //     format: "yyyy-mm-dd",
    //     todayHighlight: true,
    //     orientation: "top auto",
    //     todayBtn: true,
    //     todayHighlight: true,  
    // });

  });

$('#form-modal').submit(function(e){
  return false;
});

function Batal(){
  location.reload();
}

function validasi_modal(){
  var nama_user = $('#nama_user').val();
  var nama_supplier = $('#id_supplier').val();
  var nama_barang = $('#id_barang').val();
  var jumlah = $('#jumlah').val();
  var tanggal = $('#tgl_masuk').val();
  var no_ref = $('#no_ref').val();
  if (nama_user == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (nama_supplier == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (nama_barang == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (jumlah == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (tanggal == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (no_ref == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 }

 return true;
}

var save_method;
var table;

function add_bm() {
  save_method = 'add';
  $('#form-modal')[0].reset();
  $('#modal_form').modal('show');
}

function Simpan() {
  if(!validasi_modal()){
    return ;
  }
  var url;

  if(save_method == 'add') {
    url = '<?php echo site_url('barang_masuk/add_bm');?>';
  } else {
    url = '<?php echo site_url('barang_masuk/update_bm');?>';
  }

  $.ajax({
    url: url,
    type: "POST",
    data: $('#form-modal').serialize(),
    dataType: "JSON",
    success: function(data){
     if(!data['status']){
      swal("Gagal", data['message'], "error")
    }else{
     $('#modal_form').modal('hide');
     swal({
      title : "",
      type : "success",
      text : "Data berhasil disimpan",
      confirmButtonText : "Oke"
    }, 
    function() {
      location.reload();
    });   
   }
 },

 error: function(jqXHR, textStatus, errorThrown) {
  alert('Error Adding / Update Data');
}
});
}

function edit_bm(id){
  save_method = 'update';
  $('#form-modal')[0].reset();

  //load data dari ajax
  $.ajax({
    url: "<?php echo site_url('barang_masuk/edit_bm/');?>/"+id,
    type: "GET",
    dataType: "JSON",
    success: function(data){
      $('[name="id"]').val(data.id);
      $('[name="nama_user"]').val(data.nama_user);
      $('[name="id_supplier"]').val(data.id_supplier);
      $('[name="id_barang"]').val(data.id_barang);
      $('[name="jumlah"]').val(data.jumlah);
      $('[name="tgl_masuk"]').val(data.tgl_masuk);
      $('[name="no_ref"]').val(data.no_ref);

      $('#modal_form').modal('show');
      $('.modal-tittle').text('Edit Barang Masuk');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      alert('Error get data from ajax');
    }

  });
}

function delete_bm(id){
  swal({
    text : 'Apakah anda yakin akan menghapus data ?',
    type : 'warning',
    title : 'Hapus Data',
    showCancelButton : true,
    cancelButtonText : "Batal",
    confirmButtonText : "Hapus",
    closeOnConfirm : false,
  },function (isConfirm) {
    if (!isConfirm) return;
    $.ajax({
      url: "<?php echo site_url('barang_masuk/delete_bm');?>/"+id,
      type: "POST",
      dataType: "JSON",
      success: function(data){
        swal({
          title : "",
          type : "success",
          text : "Data berhasil di hapus",
          confirmButtonText : "Oke"
        }, 
        function() {
          location.reload();
        });
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("Error", "Data gagal dihapus !", "error");
      }
    });

  })
}

</script>
