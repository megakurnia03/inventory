<div class="container-fluid">
  <!-- Page Heading -->
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home </a>
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Transaksi
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Barang Keluar
      </li>
    </ul>
  </div>
  <div class="container-fluid">
    <!-- Page Heading -->
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onclick="add_bk()" ><i class="fas fa-plus-circle fa-sm text-white-50"></i> Tambah Barang Keluar </button>
      </div>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Barang Keluar</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>User</th>
                <th>Barang</th>
                <th>Jumlah</th>
                <th>Tanggal Keluar</th>
                <th>No Referensi</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="show_data">
              <?php
              foreach ($t_barang_keluar as $barang_keluar) {
                ?>
                <tr>
                  <td><?php echo $barang_keluar->nama_user;?></td>
                  <td><?php echo $barang_keluar->nama_barang;?></td>
                  <td><?php echo $barang_keluar->jumlah;?></td>
                  <td><?php echo date('d/m/Y',strtotime($barang_keluar->tgl_keluar)); ?></td>
                  <td><?php echo $barang_keluar->no_ref;?></td>
                  <td><?php echo $barang_keluar->keterangan;?></td>
                  <td>
                    <button class="btn btn-primary" onclick="edit_bk(<?php echo $barang_keluar->id;?>)">Edit</button>
                    <button class="btn btn-warning" onclick="delete_bk(<?php echo $barang_keluar->id;?>)">hapus</button>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>User</th>
                  <th>Barang</th>
                  <th>Jumlah</th>
                  <th>Tanggal Keluar</th>
                  <th>No Referensi</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade right" id="modal_form" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
          <div class="card shadow mb-6">
            <!--Header-->
            <div class="modal-header no-padding bg-primary">
              <h5 class="modal-tittle m-0 font-weight-bold text-light">
               Input Barang Keluar
             </h5>
           </div>
           <!--End Header-->
           <!--Modal Body-->
           <div class="modal-body no-padding">
            <form action="#" id="form-modal" method="">
              <input type="hidden" name="id" id="id">
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label class="control-label">User :</label>
                    <input type="text" name="nama_user" id="nama_user" value="<?php echo $this->session->userdata('nama')?>" class="form-control" readonly>
                  </div>
                  <div class="col-sm-6">
                    <label class="control-label">Barang :</label>
                    <select name="id_barang" id="id_barang" class="form-control">
                      <option value="">No Selected</option>
                      <?php foreach($t_barang as $row):?>
                        <option value="<?php echo $row->id;?>"><?php echo $row->nama_barang;?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-sm-3">
                    <label class="control-label">Jumlah :</label>
                    <input type="number" name="jumlah" id="jumlah" min="0" class="form-control">
                    <input type="hidden" name="stok" id="stok" min="0" class="form-control">
                  </div>
                  <div class="col-sm-4">
                    <label class="control-label">Tanggal Keluar :</label>
                    <input type="Date" name="tgl_keluar" id="tgl_keluar" value="<?php echo date('Y-m-d')?>" class="form-control">
                  </div>
                  <div class="col-sm-5">
                    <label class="control-label">No Referensi :</label>
                    <input type="text" name="no_ref" id="no_ref" class="form-control">
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <label class="control-label">Keterangan :</label>
                    <textarea class="form-control" rows="4" name="keterangan" id="keterangan"></textarea>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="form-group">
              <button type="submit" name="simpan" value="simpan" onclick="Simpan()" class="btn btn-outline-primary btn-bold"><i class="fa fa-check" aria-hidden="true"></i>Simpan</button>
              <button type="button" name="cancel" onclick="Batal()" data-dismiss="modal" class="btn btn-outline-danger btn-bold"><i class="fa fa-times" aria-hidden="true"></i>Batal</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#table').DataTable();

  $('#id_barang').change(function(){
    var id=$(this).val();
    $.ajax({
      url: "<?php echo site_url('Barang_keluar/get_jumlah_barang');?>",
      method: "POST",
      data: {id:id},
      async: true,
      dataType: 'json',
      success: function(data){

        console.log(data[0]['stok']);
        $('#jumlah').val(data[0]['stok']);
        $('#jumlah').attr('max',data[0]['stok']);
        $('#stok').val(data[0]['stok']);
       //  var html = '';
       //  var i;
       //  for(i=0; i<data.length; i++){
       //   html += '<option value='+data[i].id+'>'+data[i].nama_sub_kategori+'</option>';
       // }
       // $('#jumlah').html(html);
     }
   });
    return false;
  });

});

$('#form-modal').submit(function(e){
  return false;
});

function Batal(){
  location.reload();
}

function validasi_modal(){
  var nama_user = $('#nama_user').val();
  var nama_barang = $('#id_barang').val();
  var stok = $('#stok').val();
  var jumlah = $('#jumlah').val();
  var tanggal = $('#tgl_keluar').val();
  var no_ref = $('#no_ref').val();
  var keterangan = $('#keterangan').val();
  if (nama_user == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (nama_barang == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (jumlah == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (tanggal == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (no_ref == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 }   if (keterangan == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 }
 if (parseInt(jumlah) > parseInt(stok)) {
   swal({
    text : 'Permintaan Melebihi Stok',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 } if (parseInt(jumlah) <= 0)  {
   swal({
    text : 'Permintaan tidak boleh minus',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 }

 return true;
}

function Batal(){
  location.reload();
}

var save_method;
var table;

function add_bk() {
  save_method = 'add';
  $('#form-modal')[0].reset();
  $('#modal_form').modal('show');
}

function Simpan() {
  if(!validasi_modal()){
    return ;
  }
  var url;

  if(save_method == 'add') {
    url = '<?php echo site_url('barang_keluar/add_bk');?>';
  } else {
    url = '<?php echo site_url('barang_keluar/update_bk');?>';
  }

  $.ajax({
    url: url,
    type: "POST",
    data: $('#form-modal').serialize(),
    dataType: "JSON",
    success: function(data){
     if(!data['status']){
      swal("Gagal", data['message'], "error")
    }else{
     $('#modal_form').modal('hide');
     swal({
      title : "",
      type : "success",
      text : "Data berhasil disimpan",
      confirmButtonText : "Oke"
    }, 
    function() {
      location.reload();
    });   
   }
 },

 error: function(jqXHR, textStatus, errorThrown) {
  alert('Error Adding / Update Data');
}
});
}

function edit_bk(id){
  save_method = 'update';
  $('#form-modal')[0].reset();

  //load data dari ajax
  $.ajax({
    url: "<?php echo site_url('barang_keluar/edit_bk/');?>/"+id,
    type: "GET",
    dataType: "JSON",
    success: function(data){
      $('[name="id"]').val(data.id);
      $('[name="nama_user"]').val(data.nama_user);
      $('[name="id_barang"]').val(data.id_barang);
      $('[name="jumlah"]').val(data.jumlah);
      $('[name="tgl_keluar"]').val(data.tgl_keluar);
      $('[name="no_ref"]').val(data.no_ref);
      $('[name="keterangan"]').val(data.keterangan);

      $('#modal_form').modal('show');
      $('.modal-tittle').text('Edit Barang Keluar');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      alert('Error get data from ajax');
    }

  });
}

function delete_bk(id){
  swal({
    text : 'Apakah anda yakin akan menghapus data ?',
    type : 'warning',
    title : 'Hapus Data',
    showCancelButton : true,
    cancelButtonText : "Batal",
    confirmButtonText : "Hapus",
    closeOnConfirm : false,
  },function (isConfirm) {
    if (!isConfirm) return;
    $.ajax({
      url: "<?php echo site_url('barang_keluar/delete_bk');?>/"+id,
      type: "POST",
      dataType: "JSON",
      success: function(data){
        swal({
          title : "",
          type : "success",
          text : "Data berhasil di hapus",
          confirmButtonText : "Oke"
        }, 
        function() {
          location.reload();
        });
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("Error", "Data gagal dihapus !", "error");
      }
    });

  })
}

</script>
