<div class="container-fluid">
  <!-- Page Heading -->
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home </a>
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Barang Keluar
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Retur Barang
      </li>
    </ul>
  </div>
  <div class="container-fluid">
    <!-- Page Heading -->
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" onclick="add_retur()" ><i class="fas fa-plus-circle fa-sm text-white-50"></i> Tambah Retur </button>
      </div>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Barang Masuk</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Barang</th>
                <th>Merk</th>
                <th>Supplier</th>
                <th>Tanggal Pengembalian</th>
                <th>No Retur</th>
                <th>Jumlah</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="show_data">
              <?php
              foreach ($t_retur as $retur) {
                ?>
                <tr>
                  <td><?php echo $retur->nama_barang;?></td>
                  <td><?php echo $retur->nama_merk;?></td>
                  <td><?php echo $retur->nama_supplier;?></td>
                  <td><?php echo $retur->tgl_retur;?></td>
                  <td><?php echo $retur->no_retur;?></td>
                  <td><?php echo $retur->jumlah;?></td>
                  <td><?php echo $retur->keterangan;?></td>
                  <td>
                   <button class="btn btn-primary" onclick="edit_retur(<?php echo $retur->id;?>)">Edit</button>
                   <button class="btn btn-warning" onclick="delete_retur(<?php echo $retur->id;?>)">hapus</button>
                 </td>
               </tr>
               <?php } ?>
             </tbody>
             <tfoot>
              <tr>
               <th>Barang</th>
               <th>Merk</th>
               <th>Supplier</th>
               <th>Tanggal Pengembalian</th>
               <th>No Retur</th>
               <th>Jumlah</th>
               <th>Keterangan</th>
               <th>Aksi</th>
             </tr>
           </tfoot>
         </table>
       </div>
     </div>
   </div>
 </div>
 <div class="modal fade right" id="modal_form" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-right modal-notify modal-info" role="document">
    <div class="modal-content">
      <div class="card shadow mb-6">
        <!--Header-->
        <div class="modal-header no-padding bg-primary">
          <h5 class="modal-tittle m-0 font-weight-bold text-light">
            Retur Barang
          </h5>
        </div>
        <!--End Header-->
        <!--Modal Body-->
        <div class="modal-body no-padding">
          <form action="#" id="form-modal" method="">
            <input type="hidden" name="id" id="id">
            <div class="col-md-12">
              <div class="form-group row">
                <div class="col-sm-6">
                  <label class="control-label">Barang :</label>
                  <select name="id_barang" id="id_barang" class="form-control">
                    <option value="">No Selected</option>
                    <?php foreach($t_barang as $row):?>
                      <option value="<?php echo $row->id;?>"><?php echo $row->nama_barang;?></option>
                    <?php endforeach;?>
                  </select>
                </div>
                <div class="col-sm-6">
                  <label class="control-label">Merk :</label>
                  <input type="text" name="merk" id="merk" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group row">
                <div class="col-sm-12">
                  <label class="control-label">Supplier :</label>
                  <select name="id_supplier" id="id_supplier" class="form-control">
                    <option value="">No Selected</option>
                    <?php foreach($t_supplier as $row):?>
                      <option value="<?php echo $row->id;?>"><?php echo $row->nama_supplier;?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group row">
                <div class="col-sm-4">
                  <label class="control-label">Tanggal Pengembalian :</label>
                  <input type="Date" name="tgl_retur" id="tgl_retur" class="form-control">
                </div>
                <div class="col-sm-5">
                  <label class="control-label">No Retur :</label>
                  <input type="text" name="no_retur" id="no_retur" class="form-control">
                </div>
                <div class="col-sm-3">
                  <label class="control-label">Jumlah :</label>
                  <input type="number" name="jumlah" id="jumlah" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group row">
                <div class="col-sm-12">
                  <label class="control-label">Keterangan :</label>
                  <textarea class="form-control" rows="4" name="keterangan" id="keterangan"></textarea>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <button type="submit" name="simpan" value="simpan" onclick="Simpan()" class="btn btn-outline-primary btn-bold"><i class="fa fa-check" aria-hidden="true"></i>Simpan</button>
            <button type="button" name="cancel" onclick="Batal()" data-dismiss="modal" class="btn btn-outline-danger btn-bold"><i class="fa fa-times" aria-hidden="true"></i>Batal</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#table').DataTable();

  $('#id_barang').change(function(){
    var id=$(this).val();
    $.ajax({
      url: "<?php echo site_url('Retur_2/get_nama_merk');?>",
      method: "POST",
      data: {id:id},
      async: true,
      dataType: 'json',
      success: function(data){

        var html = '';
        var i;
        for(i=0; i<data.length; i++){
         html += '<input value='+data[i].id+'>';
       }
       $('#merk').html(html);
     }
   });
    return false;
  });

});

function showData(id) {
  $.ajax({
    url: '<?php echo site_url('Retur_2/get_nama_merk');?>',
    type : 'POST',
    data : { id : id},
    async: true,
    dataType: 'json',
    success: function(data) {
      var html = '';
      var i;
      for(i=0; i<data.length; i++){
       html += '<input value='+data[i].id+'>';
     }
     $('#merk').html(html);
   }
 });
}

function Batal(){
  location.reload();
}

$('#form-modal').submit(function(e){
  return false;
});

function Batal(){
  location.reload();
}

function validasi_modal(){
  var nama_supplier = $('#id_supplier').val();
  var nama_barang = $('#id_barang').val();
  var nama_merk = $('#merk').val();
  var jumlah = $('#jumlah').val();
  var tanggal = $('#tgl_retur').val();
  var no_retur = $('#no_retur').val();
  var keterangan = $('#keterangan').val();
  if (nama_supplier == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (nama_merk == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (nama_barang == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (jumlah == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (tanggal == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (no_retur == '') {
    swal({
      text : 'Seluruh Data Wajib Diisi',
      type : 'error',
      title : 'Tidak Bisa Menyimpan',
      showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
    return false;
  } if (keterangan == '') {
   swal({
    text : 'Seluruh Data Wajib Diisi',
    type : 'error',
    title : 'Tidak Bisa Menyimpan',
    showCancelButton : false,
        //cancelButtonText : "Batal",
        confirmButtonText : "Ok",
        closeOnConfirm : false,
      })
   return false;
 }

 return true;
}

var save_method;
var table;

function add_retur() {
  save_method = 'add';
  $('#form-modal')[0].reset();
  $('#modal_form').modal('show');
}

function Simpan() {
  if(!validasi_modal()){
    return ;
  }
  var url;

  if(save_method == 'add') {
    url = '<?php echo site_url('retur_2/add_retur');?>';
  } else {
    url = '<?php echo site_url('retur_2/update_retur');?>';
  }

  $.ajax({
    url: url,
    type: "POST",
    data: $('#form-modal').serialize(),
    dataType: "JSON",
    success: function(data){
      $('#modal_form').modal('hide');
      location.reload();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      alert('Error Adding / Update Data');
    }
  });
}

function edit_retur(id){
  save_method = 'update';
  $('#form-modal')[0].reset();

  //load data dari ajax
  $.ajax({
    url: "<?php echo site_url('retur_2/edit_retur/');?>/"+id,
    type: "GET",
    dataType: "JSON",
    success: function(data){
      $('[name="id"]').val(data.id);
      $('[name="id_barang"]').val(data.id_barang);
      $('[name="merk"]').val(data.merk);
      $('[name="id_supplier"]').val(data.id_supplier);
      $('[name="tgl_retur"]').val(data.tgl_retur);
      $('[name="no_retur"]').val(data.no_retur);
      $('[name="jumlah"]').val(data.jumlah);
      $('[name="keterangan"]').val(data.keterangan);

      $('#modal_form').modal('show');
      $('.modal-tittle').text('Edit Retur Barang');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      alert('Error get data from ajax');
    }

  });
}

function delete_retur(id){
  swal({
    text : 'Apakah anda yakin akan menghapus data ?',
    type : 'warning',
    title : 'Hapus Data',
    showCancelButton : true,
    cancelButtonText : "Batal",
    confirmButtonText : "Hapus",
    closeOnConfirm : false,
  },function (isConfirm) {
    if (!isConfirm) return;
    $.ajax({
      url: "<?php echo site_url('retur_2/delete_retur');?>/"+id,
      type: "POST",
      dataType: "JSON",
      success: function(data){
        swal({
          title : "",
          type : "success",
          text : "Data berhasil di hapus",
          confirmButtonText : "Oke"
        }, 
        function() {
          location.reload();
        });
      },
      error: function (jqXHR, textStatus, errorThrown){
        swal("Error", "Data gagal dihapus !", "error");
      }
    });

  })
}

</script>
