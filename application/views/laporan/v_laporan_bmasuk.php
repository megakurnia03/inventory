<!DOCTYPE html>
<html>
<head>
	<title>Laporan Barang Masuk</title>
</head>
<body>
	<h4 align="center">UD. WAHYU AGUNG PS<br>PET AND ANIMAL FEED STORE<br><h5 align="center">Jl. Kabupaten Sleman, Kronggahan 1, Trihanggo, Gamping, Sleman, Yogyakarta telp: (0274) 869428<hr></h5></h4>
	<h4 align="center">LAPORAN BARANG MASUK</h4>

	<table width="100%" border="0,5px" align="center">
		<thead>
			<tr>
				<th>User</th>
				<th>Supplier</th>
				<th>Barang</th>
				<th>Jumlah</th>
				<th>Tanggal Masuk</th>
				<th>No Referensi</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($t_barang_masuk as $tampil){?>
			<tr>
				<td><?php echo $tampil->nama_user;?></td>
				<td><?php echo $tampil->nama_supplier;?></td>
				<td><?php echo $tampil->nama_barang;?></td>
				<td><?php echo $tampil->jumlah;?></td>
				<td><?php echo $tampil->tgl_masuk;?></td>
				<td><?php echo $tampil->no_ref;?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<br>
	<br>
	<br>
	<table>
		<tr>
				<td style="padding-left:320px;"><p>
					Yogyakarta, <?php $tgl=date('d-m-Y')?><?php echo $tgl?><br>
					Penatausaha Persediaan<br>
					<br><br><br>
					<?php echo $this->session->userdata('nama')?><br>
					</p>
				</td>
			</tr>
		</table>
	</body>
	</html>