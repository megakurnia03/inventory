<div class="container-fluid">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i><a href="#">Home</a>
			</li>
			<li class="active">
				<i class="ace-icon fa fa-angle-double-right"></i>
			</li>
			<li class="active">Barang Masuk</li>
			<li class="active">
				<i class="ace-icon fa fa-angle-double-right"></i>
			</li>
			<li class="active">Input Barang Masuk</li>
		</ul>
	</div>
	<div class="container" style="width: 1500px;">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Form Input Barang Masuk</h6>
			</div>
			<div class="card-body">
				 <?php echo form_open('barang_masuk/add_bm') ?>
					<input type="hidden" name="id" id="id">
					<div class="col-md-12">
						<div class="form-group row">
							<div class="col-sm-6">
								<label class="control-label">User :</label>
								<select name="id_user" id="id_user" class="form-control">
									<option value=""></option>
									option
								</select>
							</div>
							<div class="col-sm-6">
								<label class="control-label">Supplier :</label>
								<select name="id_supplier" id="id_supplier" class="form-control">
									<option value=""></option>
									option
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label class="control-label">Barang :</label>
								<select name="id_barang" id="id_barang" class="form-control">
									<option value=""></option>
									option
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group row">
							<div class="col-sm-3">
								<label class="control-label">Jumlah :</label>
								<input type="number" name="jumlah" id="jumlah" class="form-control">
							</div>
							<div class="col-sm-4">
								<label class="control-label">Tanggal Masuk :</label>
								<input type="Date" name="tgl_masuk" id="tgl_masuk" class="form-control">
							</div>
							<div class="col-sm-5">
								<label class="control-label">No Referensi :</label>
								<input type="text" name="no_ref" id="no_ref" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-md-12">
								<button type="submit" id="save" name="simpan" id="simpan" class="btn btn-outline-primary btn-bold"><i class="fa fa-check"></i>Simpan</button>
								<button type="submit" name="cancel" data-dismiss="modal" class="btn btn-outline-danger btn-bold"><i class="fa fa-times" aria-hidden="true"></i>Batal</button>
					</div>
				 <?php echo form_close() ?>
			</div>
			<div class="card-footer"></div>
		</div>
	</div>
</div>