<div class="container-fluid">
  <div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        <a href="#">Home </a>
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        Barang Masuk
      </li>
      <li class="active">
        <i class="ace-icon fa fa-angle-double-right"></i>
      </li>
      <li class="active">
        List
      </li>
    </ul>
  </div>
  <div class="container-fluid">
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
          <i class="fas fa-plus-circle fa-sm text-white-50"></i>
          Tambah Data
        </button>
      </div>
    </div>
    <!--Data Tables-->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Barang Masuk</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>User</th>
                <th>Supplier</th>
                <th>Barang</th>
                <th>Jumlah</th>
                <th>Tanggal Masuk</th>
                <th>No Referensi</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="show_data">
              <?php
              foreach ($t_barang_masuk as $barang_masuk) {
                ?>
                <tr>
                  <td><?php echo $barang_masuk->nama_user;?></td>
                  <td><?php echo $barang_masuk->nama_supplier;?></td>
                  <td><?php echo $barang_masuk->nama_barang;?></td>
                  <td><?php echo $barang_masuk->jumlah;?></td>
                  <td><?php echo $barang_masuk->tgl_masuk;?></td>
                  <td><?php echo $barang_masuk->no_ref;?></td>
                  <td>
                    <button class="btn btn-primary" onclick="edit_barang(<?php echo $barang_masuk->id;?>)">Edit</button>
                    <button class="btn btn-warning" onclick="delete_barang(<?php echo $barang_masuk->id;?>)">hapus</button>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>User</th>
                  <th>Supplier</th>
                  <th>Barang</th>
                  <th>Jumlah</th>
                  <th>Tanggal Masuk</th>
                  <th>No Referensi</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#table').DataTable();
  });
</script>