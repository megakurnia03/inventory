<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_barang', 'model');
		$this->load->model('m_barang_masuk');
		$this->load->model('m_barang_keluar');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['stok'] = $this->model->get_jumlah();
		$data['panelstok'] = $this->model->panel_barang();
		$data['bmasuk'] = $this->m_barang_masuk->panel_bmasuk();
		$data['bkeluar'] = $this->m_barang_keluar->panel_bkeluar();
		$this->load->view('include/header_admin.php',$data);
		$this->load->view('dashboard/v_dashboard_admin');
		$this->load->view('include/footer');
		
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */