<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_keluar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_barang_keluar', 'model');
		$this->load->model('m_barang');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['t_barang_keluar'] = $this->model->get_all();
		$data['t_barang'] = $this->model->get_barang()->result();
		// $data['t_user'] = $this->model->get_user()->result();
		$data['stok'] = $this->m_barang->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('transaksi/v_barang_keluar.php', $data);
		$this->load->view('include/footer.php');
		
	}

	public function add_bk() {
		if ($this->input->post('stok') < $this->input->post('jumlah')) {
			
			echo json_encode(array("status" => false));
			die();
		} if ($this->input->post('jumlah') <= 0 ) {
			
			echo json_encode(array("status" => false));
			die();
		}
		$data = array(
			'nama_user' => $this->input->post('nama_user'),
			'id_barang' => $this->input->post('id_barang'),
			'jumlah' => $this->input->post('jumlah'),
			'tgl_keluar' => $this->input->post('tgl_keluar'),
			'no_ref' => $this->input->post('no_ref'),
			'keterangan' => $this->input->post('keterangan'),

		);

		$insert = $this->model->add_bk($data);
		echo json_encode(array("status" => true));
	}

	public function edit_bk($id){
		$data = $this->model->edit_bk($id);
		echo json_encode($data);
	}

	public function update_bk(){
		$data = array(
			'nama_user' => $this->input->post('nama_user'),
			'id_barang' => $this->input->post('id_barang'),
			'jumlah' => $this->input->post('jumlah'),
			'tgl_keluar' => $this->input->post('tgl_keluar'),
			'no_ref' => $this->input->post('no_ref'),
			'keterangan' => $this->input->post('keterangan'),
		);

		$this->model->update_bk(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_bk($id){
		$this->model->delete_by_id($id);
		echo json_encode(array("status" => true));
	}

	function get_jumlah_barang(){
		$id_barang = $this->input->post('id', true);
		$data = $this->model->get_jumlah_barang($id_barang)->result();
		echo json_encode($data);
	}

}

/* End of file Barang_keluar.php */
/* Location: ./application/controllers/Barang_keluar.php */