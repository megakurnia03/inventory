<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_kategori');
		$this->load->model('m_barang', 'model');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['t_kategori'] = $this->m_kategori->get_all_kategori();
		$data['stok'] = $this->model->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('masterdata/v_kategori.php', $data);
		$this->load->view('include/footer.php');
	}

	public function add_kategori() {
		$data = array(
			'nama_kategori' => $this->input->post('nama_kategori'),
		);

		$insert = $this->m_kategori->add_kategori($data);
		echo json_encode(array("status" => true));
	}

	public function ajax_edit($id){
		$data = $this->m_kategori->get_by_id($id);
		echo json_encode($data);
	}

	public function update_kategori(){
		$data = array(
			'nama_kategori' => $this->input->post('nama_kategori'),
		);

		$this->m_kategori->update_kategori(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_kategori($id){
		$this->m_kategori->delete_by_id($id);
		echo json_encode(array("status" => true));
	}


}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */