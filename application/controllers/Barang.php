<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_barang', 'model');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}


	public function index()
	{
		$data['t_barang'] = $this->model->get_all_barang();
		$data['t_kategori'] = $this->model->get_kategori()->result();
		$data['cari_kategori'] = $this->model->get_kategori()->result();
		$data['jenis'] = $this->model->get_jenis()->result();
		$data['merk'] = $this->model->get_merk()->result();
		$data['stok'] = $this->model->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('masterdata/v_barang.php', $data);
		$this->load->view('include/footer.php');

	}

	public function add_barang() {
		$barang = $this->model->get_nambar($this->input->post('nama_barang'));

		if(!empty($barang)){
			echo json_encode(array("status" => false,"message" => "Nama barang sudah ada"));
		}else{
			$data = array(
				'nama_barang' => $this->input->post('nama_barang'),
				'jenis_barang' => $this->input->post('jenis_barang'),
				'kategori' => $this->input->post('kategori'),
				'sub_kategori' => $this->input->post('sub_kategori'),
				'merk' => $this->input->post('merk'),
				'harga_satuan' => $this->input->post('harga_satuan'),
				'harga_grosir' => $this->input->post('harga_grosir'),
				'keterangan' => $this->input->post('keterangan'),

			);

			$insert = $this->model->add_barang($data);
			if ($insert) {
				echo json_encode(array("status" => true));
			}else {
			echo json_encode(array("status" => false));
			}

		}
	}


	public function ajax_edit($id){
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}

	public function update_barang(){
		$data = array(
			'nama_barang' => $this->input->post('nama_barang'),
			'jenis_barang' => $this->input->post('jenis_barang'),
			'kategori' => $this->input->post('kategori'),
			'sub_kategori' => $this->input->post('sub_kategori'),
			'merk' => $this->input->post('merk'),
			'harga_satuan' => $this->input->post('harga_satuan'),
			'harga_grosir' => $this->input->post('harga_grosir'),
			'keterangan' => $this->input->post('keterangan'),
		);

		$this->model->update_barang(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_barang($id){
		$this->model->delete_by_id($id);
		echo json_encode(array("status" => true));
	}

	//function get_jenis(){

		//$data = $this->model->get_jenis()->result();
		//echo json_encode($data);

	//}

	function get_sub_kategori(){
		$id_kategori = $this->input->post('id', true);
		$data = $this->model->get_sub_kategori($id_kategori)->result();
		echo json_encode($data);
	}

}

/* End of file Barang.php */
/* Location: ./application/controllers/Barang.php */