<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_merk');
		$this->load->model('m_barang', 'model');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['t_merk'] = $this->m_merk->get_all_merk();
		$data['stok'] = $this->model->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('masterdata/v_merk.php', $data);
		$this->load->view('include/footer.php');
	}

	public function add_merk() {
		$merk = $this->m_merk->get_namer($this->input->post('nama_merk'));
		if(!empty($merk)){
			echo json_encode(array("status" => false,"message" => "Nama Merk sudah ada"));
		} else{
			$data = array(
				'nama_merk' => $this->input->post('nama_merk'),
			);
			$insert = $this->m_merk->add_merk($data);
			if ($insert) {
				echo json_encode(array("status" => true));
			}else {
				echo json_encode(array("status" => false));
			}
		}

	}

	public function ajax_edit($id){
		$data = $this->m_merk->get_by_id($id);
		echo json_encode($data);
	}

	public function update_merk(){
		$data = array(
			'nama_merk' => $this->input->post('nama_merk'),
		);

		$this->m_merk->update_merk(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_merk($id){
		$this->m_merk->delete_by_id($id);
		echo json_encode(array("status" => true));
	}

}

/* End of file Merk.php */
/* Location: ./application/controllers/Merk.php */