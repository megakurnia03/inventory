<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_barang');
		$this->load->model('m_retur','model');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['stok'] = $this->m_barang->get_jumlah();
		$data['t_retur'] = $this->model->get_all();
		$data['t_barang'] = $this->model->get_barang()->result();
		$data['t_supplier'] = $this->model->get_supplier()->result();
		// $data['t_merk'] = $this->model->get_merk()->result();
		$this->load->view('include/header.php', $data);
		$this->load->view('retur/v_retur.php', $data);
		$this->load->view('include/footer.php');
	}

	function get_jumlah_barang(){
		$id_barang = $this->input->post('id', true);
		$data = $this->model->get_jumlah_barang($id_barang)->result();
		echo json_encode($data);
	}

	public function add_retur() {

		if ($this->input->post('stok') <= $this->input->post('jumlah')) {
			
			echo json_encode(array("status" => false));
			die();
		} if ($this->input->post('jumlah') <= 0 ) {
			
			echo json_encode(array("status" => false));
			die();
		}

		$data = array(
			'id_barang' => $this->input->post('id_barang'),
			'nama_user' => $this->input->post('nama_user'),
			'id_supplier' => $this->input->post('id_supplier'),
			'tgl_retur' => $this->input->post('tgl_retur'),
			'no_retur' => $this->input->post('no_retur'),
			'jumlah' => $this->input->post('jumlah'),
			'keterangan' => $this->input->post('keterangan'),

		);

		$insert = $this->model->add_retur($data);
		echo json_encode(array("status" => true));
	}

	public function edit_retur($id){
		$data = $this->model->edit_retur($id);
		echo json_encode($data);
	}

	public function update_retur(){
		$data = array(
			'id_barang' => $this->input->post('id_barang'),
			'nama_user' => $this->input->post('nama_user'),
			'id_supplier' => $this->input->post('id_supplier'),
			'tgl_retur' => $this->input->post('tgl_retur'),
			'no_retur' => $this->input->post('no_retur'),
			'jumlah' => $this->input->post('jumlah'),
			'keterangan' => $this->input->post('keterangan'),
		);

		$this->model->update_retur(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_retur($id){
		$this->model->delete_by_id($id);
		echo json_encode(array("status" => true));
	}

}

/* End of file Retur.php */
/* Location: ./application/controllers/Retur.php */