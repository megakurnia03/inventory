<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_masuk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_barang_masuk', 'model');
		$this->load->model('m_barang');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['t_barang_masuk'] = $this->model->get_all();
		$data['t_barang'] = $this->model->get_barang()->result();
		// $data['t_user'] = $this->model->get_user()->result();
		$data['t_supplier'] = $this->model->get_supplier()->result();
		$data['stok'] = $this->m_barang->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('transaksi/v_barang_masuk.php', $data);
		$this->load->view('include/footer.php');
	}

	public function add_bm() {
		$data = array(
			'nama_user' => $this->input->post('nama_user'),
			'id_supplier' => $this->input->post('id_supplier'),
			'id_barang' => $this->input->post('id_barang'),
			'jumlah' => $this->input->post('jumlah'),
			'tgl_masuk' => $this->input->post('tgl_masuk'),
			'no_ref' => $this->input->post('no_ref'),

		);

		$insert = $this->model->add_bm($data);
		echo json_encode(array("status" => true));
	}

	public function edit_bm($id){
		$data = $this->model->edit_bm($id);
		echo json_encode($data);
	}

	public function update_bm(){
		$data = array(
			'nama_user' => $this->input->post('nama_user'),
			'id_supplier' => $this->input->post('id_supplier'),
			'id_barang' => $this->input->post('id_barang'),
			'jumlah' => $this->input->post('jumlah'),
			'tgl_masuk' => $this->input->post('tgl_masuk'),
			'no_ref' => $this->input->post('no_ref'),
		);

		$this->model->update_bm(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_bm($id){
		$this->model->delete_by_id($id);
		echo json_encode(array("status" => true));
	}


}

/* End of file barang_masuk.php */
/* Location: ./application/controllers/barang_masuk.php */