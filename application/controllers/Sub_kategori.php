<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sub', 'model');
		$this->load->model('m_barang');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{	$data['t_sub_kategori'] = $this->model->get_all_sub();
		$data['t_kategori'] = $this->model->get_kategori()->result();
		$data['stok'] = $this->m_barang->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('masterdata/v_sub.php', $data);
		$this->load->view('include/footer.php');
	}

	public function add_sub() {
		$data = array(
			'nama_sub_kategori' => $this->input->post('nama_sub_kategori'),
			'id_kategori' => $this->input->post('id_kategori'),
		);

		
			$insert = $this->model->add_sub($data);
			if ($insert) {
				echo json_encode(array("status" => true));
			}else {
			echo json_encode(array("status" => false));
			}
	}

	public function ajax_edit($id){
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}

	public function update_sub(){
		$data = array(
			'nama_sub_kategori' => $this->input->post('nama_sub_kategori'),
			'id_kategori' => $this->input->post('id_kategori'),
		);

		$this->model->update_sub(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_sub($id){
		$this->model->delete_by_id($id);
		echo json_encode(array("status" => true));
	}

}

/* End of file Sub_kategori.php */
/* Location: ./application/controllers/Sub_kategori.php */