<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('m_login');
		$this->load->library('session');
	}
	public function index()
	{
		$this->load->view('login/v_login');
		
	}

	function proses_login(){
		$user = $this->input->post('username');
		$pass = $this->input->post('password');

		$ceklogin = $this->m_login->login($user,$pass);

		if($ceklogin){
			foreach ($ceklogin as $row) {

				$this->session->set_userdata('username', $row->username);
				$this->session->set_userdata('nama', $row->nama_user);
				$this->session->set_userdata('level', $row->level);
				$this->session->set_userdata('masuk', true);

				if ($this->session->userdata('level')=="1") {
					redirect('admin/index');
				}elseif ($this->session->userdata('level')=="2") {
					redirect('Home/index');
				}
			} 
		}else {
			$data['pesan']="Username atau Password Salah";
			$this->load->view('login/v_login', $data);
		}
	}

	function logout(){
		session_destroy();
		redirect('Page');
	}

}

/* End of file Page.php */
/* Location: ./application/controllers/Page.php */