<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_supplier');
		$this->load->model('m_barang', 'model');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['t_supplier'] = $this->m_supplier->get_all_supplier();
		$data['stok'] = $this->model->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('masterdata/v_supplier.php', $data);
		$this->load->view('include/footer.php');
	}

	public function add_supplier() {
		$data = array(
			'nama_supplier' => $this->input->post('nama_supplier'),
			'alamat' => $this->input->post('alamat'),
			'no_telp' => $this->input->post('no_telp'),
		);

		$insert = $this->m_supplier->add_supplier($data);
		echo json_encode(array("status" => true));
	}

	public function ajax_edit($id){
		$data = $this->m_supplier->get_by_id($id);
		echo json_encode($data);
	}

	public function update_supplier(){
		$data = array(
			'nama_supplier' => $this->input->post('nama_supplier'),
			'alamat' => $this->input->post('alamat'),
			'no_telp' => $this->input->post('no_telp'),
		);

		$this->m_supplier->update_supplier(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_supplier($id){
		$this->m_supplier->delete_by_id($id);
		echo json_encode(array("status" => true));
	}

}

/* End of file Supplier.php */
/* Location: ./application/controllers/Supplier.php */