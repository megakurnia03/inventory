<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_jenis');
		$this->load->model('m_barang', 'model');
		if ($this->session->userdata('masuk') == false) {
			
			redirect('Page');
		}
	}

	public function index()
	{
		$data['t_jenis_barang'] = $this->m_jenis->get_all_jenis();
		$data['stok'] = $this->model->get_jumlah();
		$this->load->view('include/header.php',$data);
		$this->load->view('masterdata/v_jenis.php', $data);
		$this->load->view('include/footer.php');
	}

	public function add_jenis() {
		$jenis = $this->m_jenis->get_namjen($this->input->post('nama_jenis'));

		if(!empty($jenis)){
			echo json_encode(array("status" => false,"message" => "Nama Jenis Hewan sudah ada"));
		}else{
			$data = array(
				'nama_jenis' => $this->input->post('nama_jenis'),
			);

			$insert = $this->m_jenis->add_jenis($data);
			if ($insert) {
				echo json_encode(array("status" => true));
			}else {
				echo json_encode(array("status" => false));
			}
		}
	}

	public function ajax_edit($id){
		$data = $this->m_jenis->get_by_id($id);
		echo json_encode($data);
	}

	public function update_jenis(){
		$data = array(
			'nama_jenis' => $this->input->post('nama_jenis'),
		);

		$this->m_jenis->update_jenis(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => true));
	}

	public function delete_jenis($id){
		$this->m_jenis->delete_by_id($id);
		echo json_encode(array("status" => true));
	}

}

/* End of file Jenis.php */
/* Location: ./application/controllers/Jenis.php */