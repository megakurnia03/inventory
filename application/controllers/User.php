<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_barang', 'model');
        if ($this->session->userdata('masuk') == false) {
            
            redirect('Page');
        }
    }

    public function index()
    {   
        $data['t_user'] = $this->m_user->get_all_user();
        $data['stok'] = $this->model->get_jumlah();
        $this->load->view('include/header_admin.php',$data);
        $this->load->view('masterdata/v_user.php', $data);
        $this->load->view('include/footer.php');
        
    }

    public function add_user() {
        $user = $this->m_user->get_username($this->input->post('username'));

        if(!empty($user)){
            echo json_encode(array("status" => false,"message" => "username sudah di pakai"));
        }else{
            $data = array(
                'nama_user' => $this->input->post('nama_user'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'alamat' => $this->input->post('alamat'),
                'no_telp' => $this->input->post('no_telp'),
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'level' => $this->input->post('level'),
                'email' => $this->input->post('email')
            );

            $insert = $this->m_user->add_user($data);
            if ($insert) {
                echo json_encode(array("status" => true));
            }else {
            echo json_encode(array("status" => false));
            }
        }
        
    }

    public function ajax_edit($id){
        $data = $this->m_user->get_by_id($id);
        echo json_encode($data);
    }

    public function update_user(){
    $data = array(
        'nama_user' => $this->input->post('nama_user'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'alamat' => $this->input->post('alamat'),
        'no_telp' => $this->input->post('no_telp'),
        'username' => $this->input->post('username'),
        'password' => $this->input->post('password'),
        'level' => $this->input->post('level'),
        'email' => $this->input->post('email'),
    );

    $this->m_user->update_user(array('id' => $this->input->post('id')), $data);
    echo json_encode(array("status" => true));
    }

    public function delete_user($id){
        $this->m_user->delete_by_id($id);
        echo json_encode(array("status" => true));
    }

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */